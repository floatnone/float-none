﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PracticeProject.Models;

namespace PracticeProject.Controllers
{
    public class SupportTicketsController : Controller
    {
        private PracticeProjectContext db = new PracticeProjectContext();

        // GET: SupportTickets
        public ActionResult Index()
        {
            var recent = db.SupportTickets
                  .OrderBy(b => b.TimeSubmited)
                  .ToList();
            return View(recent);
        }

        // GET: SupportTickets/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: SupportTickets/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "TicketsID,FirstName,LastName,Email,ReportType,UMessage,TimeSubmited")] SupportTicket supportTicket)
        {
            if (ModelState.IsValid)
            {
                db.SupportTickets.Add(supportTicket);
                db.SaveChanges();
                return RedirectToAction("Index", "Home");
            }

            return View(supportTicket);
        }

        // GET: SupportTickets/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SupportTicket supportTicket = db.SupportTickets.Find(id);
            if (supportTicket == null)
            {
                return HttpNotFound();
            }
            return View(supportTicket);
        }

        // POST: SupportTickets/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            SupportTicket supportTicket = db.SupportTickets.Find(id);
            db.SupportTickets.Remove(supportTicket);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
