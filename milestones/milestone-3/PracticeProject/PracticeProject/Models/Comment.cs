namespace PracticeProject.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Comment
    {
        [Key]
        public int CommentsID { get; set; }

        [DisplayName("Discussion Number")]
        public int DiscussionsID { get; set; }

        [DisplayName("User ID")]
        public int UsersID { get; set; }

        [Required]
        [StringLength(1000)]
        [DisplayName("Comment")]
        public string CommentContent { get; set; }

        [DisplayName("Date and Time")]
        public DateTime DateTimeOfCommentCreation { get; set; }

        public virtual Discussion Discussion { get; set; }

        public virtual User User { get; set; }
    }
}
