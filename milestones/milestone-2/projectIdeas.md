# Project Ideas (Refind)

## Questions To Answer

1. What is new/original about this idea? What are related websites/apps? (Be able to answer the question: isn’t somebody already doing this?)

2. Why is this idea worth doing? Why is it useful and not boring?

3. What are a few major features?

4. What resources will be required for you to complete this project that are not already included in the class. i.e. you already have the Microsoft stack, server, database so what else would you need? Additional API’s, frameworks or platforms you’ll need to use.

5. What algorithmic content is there in this project? i.e. what algorithm(s) will you have to develop or implement in order to do something central to your project idea? (Remember, this isn’t just a software engineering course, it is your CS degree capstone course!)

6. Rate the topic with a difficulty rating of 1-10. One being supremely easy to implement (not necessarily short though). Ten would require the best CS students using lots of what they learned in their CS degree, plus additional independent learning, to complete successfully.


==========

## Phone-a-thon

Instead of taking the initial Phone-a-thon idea where we would work directly alongside Western Oregon University, we are aiming towards a general system that would help in the facilitation of a phone-a-thon, regardless of the vendor. This idea would help avoid issues as seen by WOU’s “The Foundation” here at WOU, where they are using a Google Sheets file to store and share data with others. This is both risky, not secure, and very bare-boned. Our application would allow the storage of customer information, donation amounts, time of calls, etc.

This idea would be useful to any company, non-profit, or business that wants to do a phone-a-thon. It would provide immediate usability to our customers and could be immediately implemented after properly seeding the database.

A few of the major features would be the permissions side of it, which is an understated feature when it comes to phone-a-thons... For most Phone-a-thons, you have volunteers who are taking the calls and donations. You would not want to make every contact from a donor available to each of these volunteers. This would require logins, most likely a group login for volunteers, and separate login credentials for each of the managers of the Phone-a-thon. Another major feature would be the how easily the volunteers to input the information than to have the administrators be able to view this sensitive information in order to make follow-up calls. This would provide a quick, real-world solution that could instantly start solving problems for small non-profit foundations or companies that wish to run a Phone-a-thon.

Our first contact would be with The Foundation here at WOU and get real-life User Stories and be able to use them as our inspiration for this site. They would help give us a good idea of the information and content that they require to run a Phone-a-thon. Additional features or APIs we might require are Twilio. Twilio is a fully-programmable contact center platform that allows Programmable SMS (send/receive SMS messages), Programmable Voice (control & orchestrate phone calls), and other features as well.

There are a few algorithmic possibilities with this project. One potential algorithm would be to write an algorithm to determine when best to call specific people in follow-up years. For example, if an alumnus to a university typically gives more money in the month of December so they can write it off as a tax break, we would have an algorithm to alert or notify The Foundation that a follow-up call the next year would be ideally made it the month of December. Another algorithmic possibility would be to assign calls to volunteers. This would prevent volunteers from calling the same person multiple times, and after the call is made, it would prevent that donor from being called again. This would also prevent simultaneous phone calls being made to the same donor.

The rating for the difficulty of this project, depending on how in-depth The Foundation explains exactly what we need, but it would most like a range between 8-9.

## City Architecture

City Architecture would be a web-based platform application that allows you to notate issues in your city or neighborhood. This would be a combination of multiple things. This would allow you to mark road issues like potholes or down trees. There is an iPhone application called Waze that allows you to use navigation and social networking to alert drivers to potholes or down trees. This, however, is limited to where you are currently driving or where your directions are taking you. For our application, it would be researchable by area, and not just necessarily to where you are going.

This idea could reach across multiple platforms and all people. People, especially in larger cities, can very frequently come upon road work, work that needs to be done, road blockages, and others. This project, although available on the iOS and Android market via Waze, is only geared toward the road in which you are traveling. In Waze, it will automatically change your route to a “shorter” route that avoids the problematic areas. Our application will actually show problematic areas as well. This will give opportunities to the Department of Transportation to address the issues marked. An official Department of Transportation could also “resolve” problems. For example, if someone marks that there is a deep pothole in an area, and the Department of Transportation fills in the hole, they can mark the pothole as “filled,” and remove it from the location.

One of the major features would be user interaction. They could be identified using login credentials, and then mark locations that need assistance from either the city, Department of Transportation, etc. The city can then arrange times to send out crews to fix the issues. Both users or city officials can mark these issues as resolved once they are solved. The idea for this project would not be fueled as a navigation application, but more as a city layout that would help you identify issue areas.

The application Waze has a usable API for the project. There are also multiple other map APIs that stem from the Google Maps Platform. They have both their Maps API as well as their Geolocation API, which not only tracks the maps aspect, but actual locations such as businesses, ride-sharing, and more. Within these APIs, we would link it to our application in order to rank the severity of the issue. Once the severity is assigned, the algorithmic component is triggered.

The algorithmic components necessary for this project would be to prioritize issues when the issues are sent to local governance or when auto-generating emails through the proper channels to alert city/DOT officials.

The rating of this project would be 9. It would be a very complex application that would require a lot of time and effort to make work well.
